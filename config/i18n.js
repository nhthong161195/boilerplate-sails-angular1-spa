module.exports.i18n = {
    locales: ['en', 'ja', 'vi'],
    defaultLocale: 'en',
    updateFiles: true,
    cookie: 'lang',
    localesDirectory: '/config/locales',
};
