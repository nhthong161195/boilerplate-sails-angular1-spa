/*
* This config file is intended to be used in production environment.
* This file should only be managed by project master.
*
* Settings in this file doesn't inherit anything from 'config/env/development.js'. At the time of deploy, make a copy
* of development file and correct the configs that differ from development.
*
*
* */

module.exports = {

};
