/*
* This file is an environment file which contains settings that affect how the system works.
* This file often contains sensitive information and must NOT be committed nor expose to public.
*
* When setting up a working environment, developers request environment file from their master.
* Developer should not modify settings in this file, unless explicitly notify the master of how this change may be necessary.
*
* To override these settings to adapt the local working environment, modify the 'local.js' file instead.
*
* NOTE:
* - all config name should be upper-cased
* */

module.exports = {

    APP_TITLE: 'Boilerplate Sails SPA',
	WWW_DOMAIN: 'localhost:1992',
	
	AUTHORIZATION_SERVER: 'http://localhost:1992',
	RESOURCE_SERVER: 'http://localhost:1992',

    port: 1992,
    MONGO_SERVER: 'localhost',
    MONGO_PORT: 27017,
    MONGO_DBNAME: 'boiler-plate-sails',
	MONGO_USERNAME: '',
	MONGO_USERPASSWORD: '',

    ADMIN_EMAIL: 'admin@local',
    ADMIN_DEFAULT_PASSWORD: '123123',
	NOREPLY_EMAIL: 'noreply@local',

    JWT_KEY: 'SD4ZZNOJEHPZACEMUUYIHDXHNR6UDZYDZW5XI3WA7DDVS57AIPY6FZAR',
    JWT_EXPIRE: '1d',
    JWT_EXPIRE_SECOND: 86400,

	EMAIL_RESEND_COOLDOWN_MS: 60000,
	EMAIL_VERIFICATION_EXPIRATION: '1d',

	AWS_KEY_ID: '',
	AWS_KEY_SECRET: '',
	AWS_SES_REGION: 'us-west-2',

    GOOGLE_CLIENT_ID: '823018874292-60mi3gf3io6ikl420gtnoe3k2r4dvlnn.apps.googleusercontent.com',
	GOOGLE_CLIENT_SECRET: 'r_6HEyUuOV0SCCcTWIp6EXQX',

    SALT_ROUND: 10,
};
