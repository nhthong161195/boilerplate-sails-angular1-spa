const jwt = require('jsonwebtoken');

module.exports.policies = {
	
	//-- open all routes to controllers
	'*': true,
	
	/*
	* This policy exposes some app configs to the UserController, as the request require views that extend app.pug template, which in turn
	* requires some sails.config variables.
	* */
	ViewController: {
		getRoot: 'exposeAppConfig',
	},
	
	UserController: {
		getLogin: 'exposeAppConfig',
		getSignUp: 'exposeAppConfig'
	}
};
