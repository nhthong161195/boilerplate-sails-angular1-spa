module.exports.bootstrap = function (cb) {

    console.log('Connecting to database...');
    const mongoose = require('mongoose');
    mongoose.Promise = global.Promise;
    mongoose.connect(`mongodb://${sails.config.MONGO_USERNAME && sails.config.MONGO_USERPASSWORD ? sails.config.MONGO_USERNAME + ':' + sails.config.MONGO_USERPASSWORD + '@' : ''}${sails.config.MONGO_SERVER}:${sails.config.MONGO_PORT}/${sails.config.MONGO_DBNAME}`, {useMongoClient: true});
    mongoose.connection.on('error', err => {

        cb(err);
    });

    mongoose.connection.once('open', async () => {

        //-- db connect success, init other things
        console.log('✓ success');

        /*
        * All models are manually exposed here. Require the corresponding model to be used globally.
        * */
        global['User'] = require('../api/models/User');
        global['Auth'] = require('../api/models/Auth');
        global['OAuthToken'] = require('../api/models/OAuthToken');

        /*
        * System's standard errors which will be returned from any call, using system utils 'returnError'.
        * The error must be upper-cased and post-fixed by '_ERROR'. Only create new error if necessary, group and use the same errors as possible.
        * Error name must clearly describe the situation.
        * */
        global['Errors'] = {
            SYSTEM_ERROR: {errorName: 'SYSTEM_ERROR'},
            INPUT_ERROR: {errorName: 'INPUT_ERROR'},
            MALFORMED_REQUEST_ERROR: {errorName: 'MALFORMED_REQUEST_ERROR'},
            NOT_FOUND_ERROR: {errorName: 'NOT_FOUND_ERROR'},
            EMAIL_EXISTED_ERROR: {errorName: 'EMAIL_EXISTED_ERROR'},
            UNKNOWN_OAUTH_PROVIDER_ERROR: {errorName: 'UNKNOWN_OAUTH_PROVIDER_ERROR'},
            WRONG_PASSWORD_ERROR: {errorName: 'WRONG_PASSWORD_ERROR'},
            EMAIL_NOT_VERIFIED_ERROR: {errorName: 'EMAIL_NOT_VERIFIED_ERROR'},
            REQUEST_COOLDOWN_ERROR: {errorName: 'REQUEST_COOLDOWN_ERROR'},
            NOT_SUPPORTED_ERROR: {errorName: 'NOT_SUPPORTED_ERROR'},
            DUPLICATED_ERROR: {errorName: 'DUPLICATED_ERROR'},
            REQUEST_EXPIRED_ERROR: {errorName: 'REQUEST_EXPIRED_ERROR'},
            NOT_AUTHENTICATED_ERROR: {errorName: 'NOT_AUTHENTICATED_ERROR'},
            TOKEN_REVOKED_ERROR: {errorName: 'TOKEN_REVOKED_ERROR'},
			UNKNOWN_OAUTH_PROVIDER: {errorName: 'UNKNOWN_OAUTH_PROVIDER'},
			TOKEN_INVALID_ERROR: {errorName: 'TOKEN_INVALID_ERROR'},
			USER_STATE_ERROR: {errorName: 'USER_STATE_ERROR'},
        };

        try {
            let initResult;
            /*
            * To init a service:
            * 1. add an init function in the corresponding service file. This function is an async function.
            * 2. add the service name to the following 'services' array.
            *
            * NOTE: the services are init by order specified below, from left to right. If error occurred in one service, the bootstrap process stops and shows error.
            * */
            let services = ['SystemService', 'UserService'];


            for (let i = 0; i < services.length; i++) {
                console.log(`Init ${services[i]}...`);
                initResult = await require(`../api/services/${services[i]}`).init();
                if (!initResult.success) {
                    console.log('Error: ', initResult);
                    return cb(initResult);
                }
                console.log('✓ success');
            }

            console.log('All services had been successfully initialized. Server is running at port ' + sails.config.port);
            cb();
        }
        catch (err) {
            cb(err);
        }
    });

};
