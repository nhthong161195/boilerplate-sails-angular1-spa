/*
* Session is disable in this boilerplate, because we are using jsonwebtoken (JWT).
*
* NOTE: Sometime both session and JWT can co-exist. To enable session functionality, uncomment some content inside this file
* and remove session disable option in .sailsrc file.
* */

module.exports.session = {
	
	// secret: 'SACDGMGJCUSJRUCCSQXDXLVO7XARX2JDWDIGZUSVMNONRT6SRG7YRI4O',
	//
	// cookie: {
	// 	maxAge: 24 * 60 * 60 * 1000
	// },
	
	//adapter: 'redis',
	
	/***************************************************************************
	 *                                                                          *
	 * The following values are optional, if no options are set a redis         *
	 * instance running on localhost is expected. Read more about options at:   *
	 *                                                                          *
	 * https://github.com/visionmedia/connect-redis                             *
	 *                                                                          *
	 ***************************************************************************/
	
	//host: 'localhost',
	//port: 6379,
	// ttl: <redis session TTL in seconds>,
	// db: 0,
	// pass: <redis auth password>,
	// prefix: 'sess:',
	
};
