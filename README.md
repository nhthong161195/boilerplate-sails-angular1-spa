# Sails Angular SPA

an example Sails powered Angular SPA website


To run this project:
- npm install
- make sure that ruby & ruby-dev are installed
- install ruby 'sass' gem
- run using nodemon or npm start

Some utils:
- Convert .png image to .ico file for Website favicon: http://icoconvert.com/
