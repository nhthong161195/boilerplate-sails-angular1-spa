app.controller('LoginController', ['$scope', '$http', function ($scope, $http) {
    
    
    const ctrl = this;
    
    ctrl.onSignIn = function (googleUser) {
        $scope.$apply(function () {
            ctrl.googleUser = googleUser;
            ctrl.login({type: 'oauth', loginData: {provider: 'google', token: googleUser.getAuthResponse().id_token}});
        });
    };
    
    ctrl.registerAccount = function () {
        let query = $scope.global.utils.breakQueries(document.location.search);
        // console.log(document.location, query);
        return `/signup?redirect=${query.redirect || "/#/user/index"}`;
    };
    
    ctrl.login = function (loginData) {
        ctrl.signingIn = true;
        $http.post('/login', {
            type: loginData.type,
            loginData: loginData.loginData
        }).then(
            function (response) {
                if (response.data.success) {
                    //--
                    $scope.global.user = {
                        token: response.data.result
                    };
                    console.log('global user: ', $scope.global.user);
                    let queries = $scope.global.utils.breakQueries(window.location.search);
                    document.location.href = queries['redirect']? decodeURIComponent(queries['redirect']) : '/#/user/index';
                }
                else {
                    ctrl.signingIn = false;
                    if (loginData.type === 'oauth') {
                        let auth2 = gapi.auth2.getAuthInstance();
                        auth2.disconnect().then(function () {
                            delete ctrl.googleUser;
                        });
                    }
                    alert($scope.global.utils.errors[response.data.error.errorName]);
                }
            },
            function () {
                ctrl.signingIn = false;
                alert($scope.global.utils.errors['NETWORK_ERROR']);
            }
        );
    };
    
    window.onSignIn = ctrl.onSignIn;
    
}]);
