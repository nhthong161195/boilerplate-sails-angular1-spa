app.controller('EditUserInfoDialogController', ['$scope', '$http', '$uibModalInstance', 'options', function ($scope, $http, $modalInstance, options) {
	
	
	$scope.close = function () {
		$modalInstance.dismiss();
	};
	
	$scope.updateInfo = function () {
		$scope.global.utils.promisify(async function () {
			$scope.processing = true;
			
			let postResult = await $scope.global.utils.post(`${$scope.global.app.config.resourceServer}/api/rpc`, {
				name: 'change_user_name',
				params: {
					firstName: $scope.firstName,
					lastName: $scope.lastName
				}
			});
			
			$scope.$apply(function(){
				$scope.processing = false;
				if (!postResult.success){
					alert($scope.global.utils.errors[postResult.error.errorName]);
				}
				else{
					$modalInstance.close({firstName: $scope.firstName, lastName: $scope.lastName});
				}
			});
		});
	};
	
	
	$scope.init = function () {
		$scope.firstName = options.firstName;
		$scope.lastName = options.lastName;
	}
	
}]);
