app.controller('SignUpController', ['$scope', '$http', function ($scope, $http) {


	const ctrl = this;

	ctrl.googleSignedIn = false;

	ctrl.onSignIn = function (googleUser) {
		$scope.$apply(function () {
			ctrl.googleSignedIn = true;
			ctrl.googleUser = googleUser.getBasicProfile();
			ctrl.signUp('oauth', {provider: 'google', id_token: googleUser.getAuthResponse().id_token});
		});
	};

	ctrl.signOut = function () {
		let auth2 = gapi.auth2.getAuthInstance();
		auth2.disconnect().then(function () {
			delete ctrl.googleUser;
			$scope.$apply(function () {
				ctrl.googleSignedIn = false;
			});
		});
	};


	ctrl.signUp = function (type, signUpData) {
		ctrl.signingUp = true;
		$http.post('/signup', {
			type: type,
			signUpData: signUpData
		}).then(
			function (response) {
				if (response.data.success) {
					//-- the server has already logged the user in, do redirect
					ctrl.signingUp = false;
					$scope.global.user = {
						token: response.data.result
					};
					let queries = $scope.global.utils.breakQueries(document.location.search);
					document.location.href = queries['redirect'] || "/#/user/index";
				}
				else {
					ctrl.signingUp = false;
					alert($scope.global.utils.errors[response.data.error.errorName]);
				}
			},
			function () {
				ctrl.signingUp = false;
				alert($scope.global.utils.errors['NETWORK_ERROR']);
			}
		);
	};

	window.onSignIn = ctrl.onSignIn;

}]);
