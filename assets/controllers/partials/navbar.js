app.controller('NavbarController', ['$scope', '$uibModal', '$localStorage', function ($scope, $modal, $localStorage) {
    
    let ctrl = this;
    
    ctrl.login = function () {
        document.location.href = '/login?redirect=' + document.location.pathname + document.location.hash;
    };
    
    ctrl.logout = function () {
        delete $scope.global.user;
        //-- disconnect google
        let auth2 = gapi.auth2? gapi.auth2.getAuthInstance() : null;
        if  (auth2)
            auth2.disconnect().then(function () {
                delete ctrl.googleUser;
            });
        document.location.href = '/logout';
    };
    
    ctrl.changeLanguage = function(lang){
        Cookies.set('lang', lang);
        //-- reload page
        document.location.reload();
    };
    
    ctrl.init = function(){
        let queries = $scope.global.utils.breakQueries(document.location.hash);
        if (queries['action'] === 'login'){
            ctrl.login();
        }
        
        let btnNavbarToggle = $('#btnNavbarToggle');
        $('li.li-toggle').on('click', function(){
            if (btnNavbarToggle.is(":visible")) {
                btnNavbarToggle.click();
            }
        });
    };
    
}]);
