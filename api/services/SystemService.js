/*
* The 'SystemService' is where system-wide variables and function are declared and implemented.
* For example, AWS S3, SES, SNS... are configured here.
* */

const aws = require('aws-sdk');
const sysUtils = require('../utils/system');

module.exports = {

    init: async function(){

    	if (sails.config.AWS_KEY_ID && sails.config.AWS_KEY_SECRET) {
			aws.config.update({
				accessKeyId: sails.config.AWS_KEY_ID,
				secretAccessKey: sails.config.AWS_KEY_SECRET
			});
			global.SES = new aws.SES({region: sails.config.AWS_SES_REGION});
		}
		else{
    		console.log('- AWS config not found, email verification disabled');
		}

        return sysUtils.returnSuccess();
    },

};
