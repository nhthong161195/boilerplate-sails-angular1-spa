const Email = require('email-templates');
const path = require('path');

const sysUtils = require('../utils/system');

const getEmailFromTemplate = async function (templateName, templateData) {
	let templateDir = path.join(__dirname, '..', 'views', 'emails');
	let email = new Email({
		views: {root: templateDir},
		transport: {
			jsonTransport: true
		},
		juice: true,
		juiceResources: {
			preserveImportant: true,
			webResources: {
				relativeTo: templateDir
			}
		}
	});
	return email.render(templateName, templateData);
};

const sendMail = async function (sender, receivers, mailSubject, mailTemplate, mailData) {
	try {
		let emailContent = await getEmailFromTemplate(mailTemplate, mailData);

		let params = {
			Destination: {
				/* required */
				BccAddresses: [],
				CcAddresses: [],
				ToAddresses: []
			},

			Message: {
				/* required */
				Body: {
					/* required */
					Html: {
						Data: '', /* required */
						Charset: 'UTF-8'
					},
					Text: {
						Data: '', /* required */
						Charset: 'UTF-8'
					}
				},
				Subject: {
					/* required */
					Data: '', /* required */
					Charset: 'UTF-8'
				}
			},

			Source: null, /* required */
		};

		params.Source = sender;
		params.Destination.ToAddresses = receivers[0];
		params.Destination.CcAddresses = receivers[1];
		params.Destination.BccAddresses = receivers[2];
		params.Message.Body.Html.Data = emailContent || '';
		params.Message.Subject.Data = mailSubject || '';

		await SES.sendEmail(params).promise();
		return sysUtils.returnSuccess();
	}
	catch(err){
		console.log('sendEmail: ', err);
		return sysUtils.returnError(Errors.SYSTEM_ERROR);
	}
};

const EmailService = {

	sendEmailVerify: async function (params) {
		/*
            params: {
                email: user email to receive notification,
                link: the link to verify email
            }
        */

		return sendMail(sails.config.NOREPLY_EMAIL, [[params.email], [], []], 'Account Registration', 'verificationEmail', {link: params.link});
	},
};

module.exports = EmailService;
