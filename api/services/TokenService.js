const jwt = require('jsonwebtoken');
const sysUtils = require('../utils/system');

module.exports = {

    generateEmailVerificationToken: async function (principal, params) {
        /*
            params:{
                email: the email
            }
         */
        try {
            return new Promise((resolve) => {
                jwt.sign({email: params.email}, sails.config.JWT_KEY, {
                    expiresIn: sails.config.EMAIL_VERIFICATION_EXPIRATION
                }, (err, token) => {
                    if (err) {
                        resolve(sysUtils.returnError(Errors.SYSTEM_ERROR));
                    }
                    else {
                        resolve(sysUtils.returnSuccess({token: token}));
                    }
                });
            });
        }
        catch (err) {
            console.log('generateEmailVerificationToken:', err);
            return sysUtils.returnError(Errors.SYSTEM_ERROR);
        }
    },

    verifyEmailVerificationToken: async function (principal, params) {

        /*
            params:{
                token: the email verification token
            }
         */
        try {
            return new Promise((resolve) => {
                jwt.verify(params.token, sails.config.JWT_KEY, (err, decoded) => {
                    if (err || !decoded)
                        return resolve(sysUtils.returnError(Errors.TOKEN_INVALID_ERROR));
                    return resolve(sysUtils.returnSuccess({email: decoded.email}));
                });
            });
        }
        catch (err) {
            console.log('verifyEmailVerificationToken:', err);
            return sysUtils.returnError(Errors.SYSTEM_ERROR);
        }
    },

    verifyRefreshToken: async function (principal, params){
        /*
            params: {
                token: the refresh token to be verified
            }
         */

        try{
            let decoded = await new Promise((resolve) => {
                jwt.verify(params.token, sails.config.JWT_KEY, (err, decoded) => {
                    if (err || !decoded)
                        return resolve(null);
                    return resolve(decoded);
                });
            });

            if (!decoded)
                return sysUtils.returnError(Errors.TOKEN_INVALID_ERROR);
            return sysUtils.returnSuccess({decoded: decoded});
        }
        catch(err){
            console.log('verifyRefreshToken: ', err);
            return sysUtils.returnError(Errors.SYSTEM_ERROR);

        }
    },

    verifyAccessToken: async function (principal, params){
        /*
            params: {
                token: the access token to be verified
            }
         */

        try{
            let decoded = await new Promise((resolve) => {
                jwt.verify(params.token, sails.config.JWT_KEY, (err, decoded) => {
                    if (err || !decoded)
                        return resolve(null);
                    return resolve(decoded);
                });
            });

            if (!decoded)
                return sysUtils.returnError(Errors.TOKEN_INVALID_ERROR);
            let token = await OAuthToken.findById(decoded._id);
            if (!token)
                return sysUtils.returnError(Errors.TOKEN_REVOKED_ERROR);
            if (!token.userID)
                return sysUtils.returnError(Errors.TOKEN_INVALID_ERROR);
            let user = await User.findById(token.userID);
            if (!user)
                return sysUtils.returnError(Errors.NOT_FOUND_ERROR);
            return sysUtils.returnSuccess(user);
        }
        catch(err){
            console.log('verifyAccessToken: ', err);
            return sysUtils.returnError(Errors.SYSTEM_ERROR);
        }
    },

    generateAccessToken: async function (principal, params) {

        /*
            params:{
                refresh: boolean, whether included a refresh token for this token
                userID: the token allow access to this user's resources
                appID [optional]: the app that receives and uses this token
                scope: the scope of the token
            }
         */
        try {
            //-- save this token in history, to be revoked if necessary
            let userToken = new OAuthToken({
                userID: params.userID,
                scope: params.scope,
                appID: params.appID,
            });
            userToken = await userToken.save();

            let tokenResult = await new Promise((resolve) => {
                jwt.sign({_id: userToken._id}, sails.config.JWT_KEY, {
                    expiresIn: sails.config.JWT_EXPIRE
                }, (err, token) => {
                    if (err) {
                        resolve(sysUtils.returnError(Errors.SYSTEM_ERROR));
                    }
                    else {
                        resolve(sysUtils.returnSuccess({token:token}));
                    }
                });
            });

            if (!tokenResult.success)
                return tokenResult;

            let result = {
                access_token: tokenResult.result.token
            };

            if (params.refresh){
                let refreshTokenResult = await new Promise((resolve) => {
                    jwt.sign({
                        tokenID: userToken._id//-- the token of which this token will refresh
                    }, sails.config.JWT_KEY, {
                        //-- refresh token has no expire, but will be revoked after being used
                    }, (err, token) => {
                        if (err) {
                            resolve(sysUtils.returnError(Errors.SYSTEM_ERROR));
                        }
                        else {
                            resolve(sysUtils.returnSuccess({token:token}));
                        }
                    });
                });
                if (!refreshTokenResult.success)
                    return refreshTokenResult;
                result.refresh_token = refreshTokenResult.result.token;
            }
            return sysUtils.returnSuccess(result);
        }
        catch (err) {
            console.log('generateAccessToken:', err);
            return sysUtils.returnError(Errors.SYSTEM_ERROR);
        }
    },

};
