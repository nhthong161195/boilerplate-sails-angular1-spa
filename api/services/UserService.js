/*
* This is how a Service is factored.
*
* Note that the service object is declared first, then exported by calling module.exports.
* */

const GoogleAuth = require('google-auth-library');
const sysUtils = require('../utils/system');

const UserService = {
	
	init: async function () {
		
		/*
        * This sample code creates an admin user of this system.
        *
        * NOTE: the init code runs each time the server restarts. Make sure that this code
        * checks whether something has been done before.
        * */
		
		let admin = await User.findOne({userClass: User.constants.UserClass.SUPER_ADMIN_USER});
		/* create admin user if not existed */
		if (!admin) {
			admin = new User({
				email: sails.config.ADMIN_EMAIL,
				userClass: [User.constants.UserClass.NORMAL_USER, User.constants.UserClass.ADMIN_USER, User.constants.UserClass.SUPER_ADMIN_USER],
				emailVerified: true,
			});
			
			/* assign save() result to the same object to obtain _id attribute */
			admin = await admin.save();
			/* admin object now has _id attribute */
			
			//-- create auth
			let passwordHash = await sysUtils.hash(sails.config.ADMIN_DEFAULT_PASSWORD);
			
			let auth = new Auth({
				userID: admin._id,
				authMethod: Auth.constants.AuthMethod.AUTH_PASSWORD,
				extra1: passwordHash,
			});
			
			await auth.save();
		}
		
		return sysUtils.returnSuccess();
	},
	
	registerNewUser: async function (principal, params) {
		
		/*
        * If the service function is not exposed by BatchController or other API, then there is no need to
        * write the API doc notation. However, the params of the function must be described.
        *
        * Every function of each service must has two params:
        * - 'principal' tells the function who is calling. This principal object is either session-cached
        * principal, or on-the-fly created when parsing JWT token in middleware.
        * - 'params' must be of type object, containing the necessary inputs for the function to work.
        *
        * A service function's logic MUST be wrapped inside a try/catch block. In case of error, the error must be logged inside catch block
        * along with name of the function.
        * console.log in function logic is allow in testing, but each log must have a identifying string along with it.
        * After the log is no more used, it's recommended to comment out the log without removing it.
        *
        *
        * A service function returns either a standard error
        * { success: false, error: {errorName: 'NOT_FOUND_ERROR'} }
        * or a success object
        * { success: true, result: {object containing info} }
        *
        *
        * NOTE: the 'result' filed in success object must have type Object, for ease of adding or removing
        * returned info.
        * */
		
		/*
            params: {
                authMethod: auth,
                authData:{
                    email,
                    password [if auth === AUTH_PASSWORD]
                }
            }
         */
		try {
			let user = await User.findOne({email: params.authData.email});
			if (user)
				return sysUtils.returnError(Errors.EMAIL_EXISTED_ERROR);
			
			//-- create new user, set emailVerified = true if authMethod is OAuth or SES config is not available
			user = new User({
				email: params.authData.email,
				emailVerified: params.authMethod === Auth.constants.AuthMethod.AUTH_OAUTH || !global.SES
			});
			user = await user.save();
			
			let auth = null;
			
			/*
			* NOTE:
			* We wrap 'case' body inside {} brackets to isolate all inner variables, so that they won't conflict with other variables in other cases.
			* */
			
			switch (params.authMethod) {
				case Auth.constants.AuthMethod.AUTH_OAUTH: {
					auth = new Auth({
						userID: user._id,
						authMethod: Auth.constants.AuthMethod.AUTH_OAUTH
					});
					auth = await auth.save();
					user = await user.save();
					return sysUtils.returnSuccess({user: user});
				}
				
				case Auth.constants.AuthMethod.AUTH_PASSWORD: {
					auth = new Auth({
						userID: user._id,
						authMethod: Auth.constants.AuthMethod.AUTH_PASSWORD,
						extra1: await sysUtils.hash(params.authData.password),
					});
					auth = await auth.save();
					if (global.SES) await UserService.resendVerifyEmail({email: user.email});
					return sysUtils.returnSuccess({user: user});
				}
				
				default:
					return sysUtils.returnError(Errors.NOT_SUPPORTED_ERROR);
			}
		}
		catch (err) {
			console.log('registerNewUser: ', err);
			return sysUtils.returnError(Errors.SYSTEM_ERROR);
		}
	},
	
	loginUser: async function (principal, params) {
		
		/*
            params: {
                authMethod: oauth || password,
                authData:{
                    email, password [if auth === AUTH_PASSWORD],
                    provider, token [if auth === AUTH_OAUTH]
                }
            }
            return: user info
         */
		try {
			let loginResult = null;
			
			switch (params.authMethod) {
				case Auth.constants.AuthMethod.AUTH_OAUTH:
				case 'oauth': {
					/*
					* you can add different cases for the same logic.
					* */
					switch (params.authData.provider) {
						case 'google':
							let auth = new GoogleAuth;
							let client = new auth.OAuth2(sails.config.GOOGLE_CLIENT_ID, '', '');
							let tokenResult = await new Promise((resolve) => {
								client.verifyIdToken(params.authData.token, sails.config.GOOGLE_CLIENT_ID, (err, login) => {
									if (err)
										return resolve(sysUtils.returnError(Errors.NOT_FOUND_ERROR));
									return resolve(sysUtils.returnSuccess({login: login}));
								});
							});
							if (!tokenResult.success)
								return tokenResult;
							//-- check for user in db
							let user = await User.findOne({email: tokenResult.result.login._payload.email});
							if (!user)
								loginResult = sysUtils.returnError(Errors.NOT_FOUND_ERROR);
							else
								loginResult = sysUtils.returnSuccess({user: user});
							break;
						
						case 'facebook':
						default:
							loginResult = sysUtils.returnError(Errors.UNKNOWN_OAUTH_PROVIDER);
							break;
					}
					break;
				}
				
				case Auth.constants.AuthMethod.AUTH_PASSWORD:
				case 'password': {
					let user = await User.findOne({email: params.authData.email});
					if (!user)
						loginResult = sysUtils.returnError(Errors.NOT_FOUND_ERROR);
					else {
						let auth = await Auth.findOne({
							userID: user._id,
							authMethod: Auth.constants.AuthMethod.AUTH_PASSWORD
						});
						if (!auth) {
							loginResult = sysUtils.returnError(Errors.NOT_FOUND_ERROR);
						}
						else {
							let result = await sysUtils.compare(params.authData.password, auth.extra1);
							if (!result)
								loginResult = sysUtils.returnError(Errors.WRONG_PASSWORD_ERROR);
							else
								loginResult = sysUtils.returnSuccess({user: user});
						}
					}
					break;
				}
				
				default:
					loginResult = sysUtils.returnError(Errors.NOT_SUPPORTED_ERROR);
					break;
			}
			
			return loginResult;
		}
		catch (err) {
			console.log('loginUser: ', err);
			return sysUtils.returnError(Errors.SYSTEM_ERROR);
		}
	},
	
	resendVerificationEmail: async function (params) {
		
		/**
		 * @api {post} resend_verification_email Resend verification email
		 * @apiGroup User
		 * @apiSuccessExample {Object} responseBody
		 * {
         *   "success": true,
         *   }
		 */
		
		try {
			//-- if SES is not setup then return not supported
			if (!global.SES)
				return sysUtils.returnError(Errors.NOT_SUPPORTED_ERROR);
			
			let user = principal.user;
			
			if (user.emailVerified)
				return sysUtils.returnError(Errors.USER_STATE_ERROR);
			
			//-- check cooldown
			if (user.emailResendCooldown && (user.emailResendCooldown.getTime() > Date.now()))
				return sysUtils.returnError(Errors.REQUEST_COOLDOWN_ERROR);
			
			//-- generate verifyEmailToken & link
			let tokenResult = await TokenService.generateEmailVerificationToken({email: user.email});
			if (!tokenResult.success)
				return tokenResult;
			
			user.emailResendCooldown = Date.now() + sails.config.EMAIL_RESEND_COOLDOWN_MS;
			await user.save();
			
			// -- send mail
			return EmailService.sendEmailVerify({
				email: user.email,
				link: sails.config.AUTHORIZATION_SERVER + '/verify-email?token=' + tokenResult.result.token
			});
		}
		catch (err) {
			console.log('resendVerificationEmail: ', err);
			return sysUtils.returnError(Errors.SYSTEM_ERROR);
		}
	},
	
	getUserInfo: async function (principal, params) {
		/**
		 * @api {post} get_user_info Get user info
		 * @apiGroup User
		 * @apiSuccessExample {Object} responseBody
		 * {
         *   "success": true,
         *    "result": {
         *        "_id": "5a0be58b5008cb3dc30ea973",
         *        "email": "maria.ozaki@gmail.com",
         *        "emailVerified": true,
         *        "createdAt": "2017-11-15T06:58:19.315Z",
         *        "accountStatus": 0,
         *        "userClass": [
         *            'NORMAL_USER'
         *        ],
         *        "__v": 0
         *    }
         *   }
		 */
		try {
			let user = principal.user;
			let userObj = await User.findById(user._id);
			if (!userObj)
				return sysUtils.returnError(Errors.NOT_FOUND_ERROR);
			
			return sysUtils.returnSuccess({user: userObj});
		}
		catch (err) {
			console.log('getUserInfo: ', err);
			return sysUtils.returnError(Errors.SYSTEM_ERROR);
		}
	},
	
	changeUserName: async function (principal, params) {
		/**
		 * @api {post} change_user_name Change user name
		 * @apiGroup RPC
		 * @apiSuccessExample {Object} responseBody
		 * {
         *   "success": true
         *   }
		 */
		try {
			let user = principal.user;
			let userObj = await User.findById(user._id);
			
			if (!userObj)
				return sysUtils.returnError(Errors.NOT_FOUND_ERROR);
			
			userObj.firstName = params.firstName;
			userObj.lastName = params.lastName;
			
			await userObj.save();
			
			return sysUtils.returnSuccess();
		}
		catch (err) {
			console.log('changeUserName: ', err);
			return sysUtils.returnError(Errors.SYSTEM_ERROR);
		}
	},
};

module.exports = UserService;

