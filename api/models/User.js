
const mongoose = require('mongoose');

const constants = {
    /*
    * Constants are used to define enum for database fields.
    * NOTE: to avoid conflict and support for database readability, enum should be of type String, and enum name should be capitalized.
    * */
    UserClass: {
        NORMAL_USER: 'NORMAL_USER',
        ADMIN_USER: 'ADMIN_USER',
		SUPER_ADMIN_USER: 'SUPER_ADMIN_USER'
    }
};


/*
* NOTE: Be careful and make sure to choose the correct properties when adding or removing model's field: unique, required, default, sparse...
* Some mongoose types: Number, String, Array, Mixed, Date.
* Currency field should be of type String and processed by BigNumber library.
* */

const userSchema = new mongoose.Schema({
	/*
	* There must be at least an human-friendly and unique field used to distinguish users.
	* */
    email: {type: String, required: true, unique: true},

	/*
	* All field names must be camel-cased with correct English. Please learn English.
	* */
	firstName: {type: String},
	lastName: {type: String},
    emailVerified: {type: Boolean, required: true, default: false},
	emailResendCooldown: {type: Date, default: Date.now},
    /*
    * Mongoose 'spare' option, along with unique option, defined a field that is either null or unique.
    * */
    phoneNumber: {type: String, unique: true, sparse: true},

    /*
    * This is how constants are used inside model
    * */
    userClass: {type: Array, default: [constants.UserClass.NORMAL_USER], required: true},

    /*
    * createdAt field should be keep in each model, for future use
    * */
    createdAt: {type: Date, required: true, default: Date.now},
});

/*
* Models must be manually public-export in 'config/bootstrap.js' file. Then in code they can be referred by their names, e.g User.
* Constants can therefore be referred as User.constants.
* */

module.exports = mongoose.model('User', userSchema, 'user');
module.exports.constants = constants;
