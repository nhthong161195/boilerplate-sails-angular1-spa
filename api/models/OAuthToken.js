"use strict";
const mongoose = require('mongoose');

const constants= {
	TokenAccessScope: {
		TOKEN_SCOPE_APP_ACCESS: 0,
		TOKEN_SCOPE_BASIC_ACCESS: 1,
		TOKEN_SCOPE_MEDIUM_ACCESS: 2,
		TOKEN_SCOPE_FULL_ACCESS: '0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF',
	}
};

const oAuthTokenSchema = new mongoose.Schema({
    userID: {type: mongoose.Schema.ObjectId, ref: 'User'},
    scope: {type: String, required: true},
    createdAt: {type: Date, required: true, default: Date.now},
});

module.exports = mongoose.model('OAuthToken', oAuthTokenSchema, 'oAuthToken');
module.exports.constants = constants;
