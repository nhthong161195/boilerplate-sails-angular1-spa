const jwt = require('jsonwebtoken');
const sysUtils = require('../utils/system');

module.exports = function (req, res, next) {
	/*
	* Every call to /api/ endpoint must contains a token. This middleware will parse that token.
	* */
	sysUtils.promisify(async () => {
		if (req.body.token) {
			let decoded = await new Promise((resolve) => {
				jwt.verify(req.body.token, sails.config.JWT_KEY, function (err, decoded) {
					if (!err && decoded) {
						resolve(decoded);
					}
					else {
						resolve(null);
					}
				});
			});
			
			if (!decoded) return sysUtils.returnError(Errors.TOKEN_INVALID_ERROR);
			//- get token from decoded id
			let token = await OAuthToken.findById(decoded._id);
			if (!token)
				return sysUtils.returnError(Errors.TOKEN_REVOKED_ERROR);
			
			let user;
			if (token.userID) user = await User.findById(token.userID);
			if (!user)
				return sysUtils.returnError(Errors.NOT_FOUND_ERROR);
			
			req.principal = {user: user};
			return sysUtils.returnSuccess();
		}
		else {
			return sysUtils.returnError(Errors.NOT_AUTHENTICATED_ERROR);
		}
	}).then(() => {
		next();
	}).catch(err => {
		res.json(err);
	});
};
