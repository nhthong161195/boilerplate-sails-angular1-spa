/*
* This Policies file is intended to be used for BatchController.
*
* */

const sysUtils = require('../utils/system');

const Policies = {
	isAuthenticated: async function(principal, params){
		return principal.user?
			sysUtils.returnSuccess() : sysUtils.returnError(Errors.NOT_AUTHENTICATED_ERROR);
	},
};

module.exports = Policies;
