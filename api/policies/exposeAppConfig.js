module.exports = function(req, res, next){
	res.locals.appTitle = sails.config.APP_TITLE;
	res.locals.googleSigninID = sails.config.GOOGLE_CLIENT_ID;
	res.locals.defaultLocale = sails.config.i18n.defaultLocale;
	res.locals.authorizationServer = sails.config.AUTHORIZATION_SERVER;
	res.locals.resourceServer = sails.config.RESOURCE_SERVER;
	next();
};
