const GoogleAuth = require('google-auth-library');
const sysUtils = require('../utils/system');

module.exports = {

	getLogin: async function (req, res) {
		/*
		* The login view extends app.pug directly, which requires some variables to populate.
		* We can inject those variables here in 'render' method, or create a policy which does that.
		* In this project, we use exposeAppConfig.js policy to apply for getLogin and getSignUp method.
		* */
		return res.render('./login', {
			/*
				appTitle: sails.config.APP_TITLE,
	        	googleSigninID: sails.config.GOOGLE_CLIENT_ID,
	        	defaultLocale: sails.config.i18n.defaultLocale
			*/
		});
	},


	getSignUp: async function (req, res) {
		return res.render('./signup');
	},

	getLogout: async function (req, res) {

		/*
		* If session is used, remove the req.session.principal, and any other relevant fields.
		* The req.session object itself should be kept.
		* This implementation uses JWT, so the following session manipulating command has been commented out.
		*
		* NOTE: user info should only be saved inside req.session.principal for ease of manipulating.
		* */

		//delete req.session.principal;
		return res.redirect('/login');
	},

	postLogin: async function (req, res) {

		/**
		 * @api {post} /login Login
		 *
		 * @apiName Login
		 *
		 * @apiGroup Authentication
		 *
		 * @apiDescription
		 * Login to the system.
		 *
		 * @apiParam {String} type Either 'password' or 'oauth'
		 * @apiParam {Object} loginData
		 * @apiParam {String} loginData.email If type is 'password', then this is the user's login email.
		 * @apiParam {String} loginData.password If type is 'password', then this is the user's login password.
		 * @apiParam {String} loginData.provider If type is 'oauth', then this is the token provider. Currently, only 'google' is supported.
		 * @apiParam {String} loginData.token If type is 'oauth', then this is the token provided by the provider.
		 *
		 * @apiSuccess {Object} The success object whose 'result' contains user token.
		 */
		try {
			let data = req.body;
			let loginResult = await UserService.loginUser({}, {authMethod: data.type, authData: data.loginData});
			if (!loginResult.success)
				return res.json(loginResult);

			let tokenResult = await TokenService.generateAccessToken({}, {
				scope: OAuthToken.constants.TokenAccessScope.TOKEN_SCOPE_FULL_ACCESS,
				userID: loginResult.result.user._id
			});

			if (!tokenResult.success)
				return res.json(tokenResult);

			/*
			* SESSION SAVING
			* The following command will be used to save the login session.
			* Again, user session info should be saved in one object, for ease of manipulating.
			* */
			//req.session.principal = {user: loginResult.result.user};

			return res.json(tokenResult);
		}
		catch (err) {
			console.log('postLogin: ', err);
			return res.json(sysUtils.returnError(Errors.SYSTEM_ERROR));
		}
	},

	postSignUp: async function (req, res) {

		/**
		 * @api {post} /signup SignUp
		 *
		 * @apiName SignUp
		 *
		 * @apiGroup Authentication
		 *
		 * @apiDescription
		 * Sign up an user to the system
		 *
		 * @apiParam {String} type Either 'oauth' or 'password'.
		 * @apiParam {Object} signUpData the data used for user registration.
		 * @apiParam {String} signUpData.email If type is 'password', then this is the email used for user registration and receiving verification link.
		 * @apiParam {String} signUpData.password If type is 'password', then this is the password used for user registration.
		 * @apiParam {String} signUpData.provider If type is 'oauth', then this is the oauth provider. Currently, only 'google' is supported.
		 * @apiParam {String} signUpData.token If type is 'oauth', then this is the token issued by oauth provider. The token must include email information that will be used for user registration.
		 *
		 * @apiSuccess {Object} successResult the success result object contains a 'success' field that has value of 'true'.
		 */

		try {
			let data = req.body;
			if (!data || !data.type || !data.signUpData)
				return res.json(sysUtils.returnError(Errors.MALFORMED_REQUEST_ERROR));

			let registerUserCallback = async function (data) {
				let userResult = await UserService.registerNewUser({}, data);
				if (!userResult.success)
					return userResult;

				let tokenResult = await TokenService.generateAccessToken({}, {
					scope: OAuthToken.constants.TokenAccessScope.TOKEN_SCOPE_FULL_ACCESS,
					userID: userResult.result.user._id
				});

				//-- save to session
				/*
				if (tokenResult.success)
					req.session.user = {token: tokenResult.result};
				*/

				return tokenResult;
			};

			let registerUserData = null;

			switch (data.type) {
				case 'oauth':
					switch (data.signUpData.provider) {
						case 'google':
							let auth = new GoogleAuth;
							let client = new auth.OAuth2(sails.config.GG_CLIENT_ID, '', '');
							client.verifyIdToken(data.signUpData.id_token, sails.config.GG_CLIENT_ID, (err, login) => {
								if (err) {
									res.json(sysUtils.returnError(Errors.TOKEN_INVALID_ERROR));
								}
								else {
									registerUserData = {
										authMethod: Auth.constants.AuthMethod.AUTH_OAUTH,
										authData: {
											email: login._payload.email
										}
									};

									registerUserCallback(registerUserData).then(result => {
										res.json(result);
									});
								}
							});
							break;

						case 'facebook':
							res.json(Errors.NOT_SUPPORTED_ERROR);
							break;
					}
					break;

				case 'password':
					if (!data.signUpData.email || !data.signUpData.password)
						return res.json(sysUtils.returnError(Errors.MALFORMED_REQUEST_ERROR));

					registerUserData = {
						authMethod: Auth.constants.AuthMethod.AUTH_PASSWORD,
						authData: {
							email: data.signUpData.email,
							password: data.signUpData.password
						}
					};

					registerUserCallback(registerUserData).then(result => {
						res.json(result);
					});
					break;

				default:
					break;

			}
		}
		catch (err) {
			console.log('postSignUp: ', err);
			return res.json(sysUtils.returnError(Errors.SYSTEM_ERROR));
		}
	},
	
	getVerifyEmail: async function (req, res) {
		"use strict";
		
		let callback = function (result) {
			res.render('./email-verification-confirmation', result);
		};
		
		if (!req.query.token)
			return callback(sysUtils.returnError(Errors.MALFORMED_REQUEST_ERROR));
		
		let verifyResult = await TokenService.verifyEmailVerificationToken({token: req.query.token});
		if (verifyResult.success) {
			let user = await User.findOne({email: verifyResult.result, emailVerified: false});
			if (!user)
				return callback(sysUtils.returnError(Errors.NOT_FOUND_ERROR));
			
			user.emailVerified = true;
			await user.save();
		}
		return callback(verifyResult);
	},
};
