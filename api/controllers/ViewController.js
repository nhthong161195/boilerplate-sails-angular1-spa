
/*
* This controller is especially used for serving static view file to angular routers.
* These views can be raw html or template. They can also be served at 'config/routes.js' level.
* */

module.exports = {
    getRoot: function(req, res){
        
        res.view('app', {
            /*
            *  You can append anything to this object to populate them to view, including sails.config parameters.
            *  NOTE: be aware not to expose sensitive information in sails.config (secret key, app api key...)
            *
            *  As we have a policy that populate these info to res.locals, we don't need the following configs anymore.
            * */
            /*
            appTitle: sails.config.APP_TITLE,
            googleSigninID: sails.config.GOOGLE_CLIENT_ID,
	        defaultLocale: sails.config.i18n.defaultLocale
             */
        });
    }
};
