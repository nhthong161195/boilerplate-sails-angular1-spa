const PO = require('../policies/Policies');
const sysUtils = require('../utils/system');

/*
* Actions are params to the 'name' of RPC and BATCH api call.
* Each action has an unique action id which will be used to check for token permission.
* Token permission is the result of 'OR-ING' the required action ids.
*
* NOTE: This implementation uses 128bit integer to represent action ids, which means there will only be a maximum of 128 different actions that can co-exist.
* BigNumber library is used to handle these ids.
* */
const actions = {
	//-- user related actions
	'get_user_info': {
		/* id is used to distinguish actions and determine token permission. This is intended to use for OAuth implementation.*/
		id: '0x00000000000000000000000000000000',
		/* policies is an array of policy function names that are defined in 'api/policies/Policies.js' file. If police check failed then a default error is returned.*/
		policies: [PO.isAuthenticated],
		/* validation is checked before entering the action. For now only required validation is available. All the field inside required must not be null nor undefined */
		validation: {required: []},
		/* action must points to a service function */
		action: UserService.getUserInfo,
	},
	
	'resend_verification_email': {
		id: '0x00000000000000000000000000000001',
		policies: [PO.isAuthenticated],
		validation: {required: []},
		action: UserService.resendVerificationEmail,
	},

	'change_user_name': {
		id: '0x00000000000000000000000000000002',
		policies: [PO.isAuthenticated],
		validation: {required: ['firstName', 'lastName']},
		action: UserService.changeUserName,
	},
};

let policiesCheck = async function (policies, principal, params) {
	for (let i = 0; i < policies.length; i++) {
		let result = await policies[i](principal, params);
		if (!result.success)
			return result;
	}
	return sysUtils.returnSuccess();
};

let runOneCommand = async function (principal, commandName, params) {

	//-- validation check
	if (!commandName || !params || !actions[commandName] || !actions[commandName].action) {
		return sysUtils.returnError(Errors.MALFORMED_REQUEST_ERROR);
	}
	if (actions[commandName].validation) {
		if (actions[commandName].validation.required && actions[commandName].validation.required.length) {
			let missing = actions[commandName].validation.required.some(attr => {
				if (params[attr] === undefined || params[attr] === null) {
					return true;
				}
			});
			if (missing) return sysUtils.returnError(Errors.MALFORMED_REQUEST_ERROR);
		}
	}

	//-- token scope check
	if (principal.user && principal.user.scope) {
		if (_.SCOPE_AUTHORIZATION[principal.user.scope].indexOf(commandName) < 0)
			return sysUtils.returnError(Errors.NOT_AUTHORIZED_ERROR);
	}

	//-- policy check
	if (actions[commandName].policies && actions[commandName].policies.length) {
		let policiesResult = await policiesCheck(actions[commandName].policies, principal, params);
		if (!policiesResult.success)
			return policiesResult;
	}

	//-- run the command
	return actions[commandName].action(principal, params);
};

module.exports = {

	rpc: function (req, res) {
		/**
		 * @api {post} /api/rpc RPC
		 *
		 * @apiGroup RPC
		 *
		 * @apiParam {String} token The token received from authorization server.
		 * @apiParam {String} name Name of the rpc method
		 * @apiParam {Object} params Object containing params of this method
		 *
		 * @apiParamExample {json} Request body
		 *   {
         *      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6MywidXNlcklEIjoiNWEwYmU1OGI1MDA4Y2IzZGMzMGVhOTczIiwiaWF0IjoxNTEwNzM2NzUzLCJleHAiOjE1MTA5OTU5NTN9.iWRCsiYJ3b2SIB9ulo33WzQPmQ7cehNC1Hv6Z7KZ77k",
         *      "name": "get_user_info",
         *      "params": {
         *      }
         *   }
		 */

		console.log(JSON.stringify(req.body));
		new Promise(async (resolve, reject) => {
			try {
				req.principal.req = req;
				req.principal.res = res;
				return resolve(await(runOneCommand(req.principal, req.body.name, req.body.params)));
			}
			catch (err) {
				return reject(err)
			}
		}).then(
			function (result) {
				if (result && !result.success) console.log(result);
				if (result) res.json(result);
			},
			function (err) {
				console.log(err);
				res.json(sysUtils.returnError(Errors.SYSTEM_ERROR));
			}
		);
	},

	batch: function (req, res) {
		/**
		 * @api {post} /api/batch BATCH
		 *
		 * @apiGroup RPC
		 *
		 * @apiParam {String} token The token received from authorization server.
		 * @apiParam {Object} options Options of request
		 * @apiParam {Boolean} options.serial Whether to run the commands serially or parallel
		 * @apiParam {Boolean} options.serialStop If options.serial is true, then stop the rest of commands if the previous one had failed. No rollback is currently support.
		 * @apiParam {Object[]} commands Array of command objects
		 *
		 * @apiParamExample {json} Request body
		 *   {
         *      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6MywidXNlcklEIjoiNWEwYmU1OGI1MDA4Y2IzZGMzMGVhOTczIiwiaWF0IjoxNTEwNzM2NzUzLCJleHAiOjE1MTA5OTU5NTN9.iWRCsiYJ3b2SIB9ulo33WzQPmQ7cehNC1Hv6Z7KZ77k",
         *      "options": {
         *          "serial": true
         *      },
         *      "commands": [
         *          {
         *              "name": "get_user_info",
         *              "params": {
         *                  "_id": "59b78b7117fcd042d067501f"
         *              }
         *          },
         *          {
         *              "name": "change_user_name",
         *              "params": {
         *                  "_id": "59b78b7117fcd042d067501f",
         *                  "firstName": "thong",
         *                  "lastName": "dx"
         *              }
         *          }
         *      ]
         *   }
		 */

		console.log(JSON.stringify(req.body));
		new Promise(async (resolve, reject) => {
			try {
				req.principal.req = req;
				req.principal.res = res;

				if (!req.body.options || !req.body.commands || req.body.commands.length > sails.config.BATCH_MAX_COMMAND_NUMBER)
					return resolve(sysUtils.returnError(Errors.MALFORMED_REQUEST_ERROR));

				let results = [];
				if (req.body.options.serial) {
					for (let cIndex = 0; cIndex < req.body.commands.length; cIndex++) {
						let result = await runOneCommand(req.principal, req.body.commands[cIndex].name, req.body.commands[cIndex].params);
						if (!result.success && req.body.options.serialStop)
							return resolve(sysUtils.returnError(Errors.EXECUTION_HALTED_ERROR));
						else {
							result.name = req.body.commands[cIndex].name;
							results.push(result);
						}
					}
					return resolve(sysUtils.returnSuccess(results));
				}
				else {
					let promises = [];
					req.body.commands.forEach(command => {
						promises.push(runOneCommand(req.principal, command.name, command.params));
					});
					let results = await Promise.all(promises);
					for (let cIndex = 0; cIndex < req.body.commands.length; cIndex++) {
						results[cIndex].name = req.body.commands[cIndex].name;
					}
					return resolve(sysUtils.returnSuccess(results));
				}

			}
			catch (err) {
				return reject(err)
			}
		}).then(
			function (result) {
				if (result && !result.success) console.log(result);
				if (result) res.json(result);
			},
			function (err) {
				console.log(err);
				res.json(sysUtils.returnError(Errors.SYSTEM_ERROR));
			}
		);
	}
};
